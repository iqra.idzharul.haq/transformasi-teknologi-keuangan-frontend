import { HttpService } from "./HttpService"

export const ENDPOINT = {
  GET_USER : "user",
  ACTIVATE_USER: "user/activate",
  GENERATE_USER: "user/generate",
  SIGNIN: "signin",
  SIGNIN_STOCKIES: "signin/stockies",
  DOWNLINES: "user/downline",
  GET_TRANSACTION_BY_USER: "transaction",
  GET_TRANSACTION_BY_STOCKIES: "transaction/stockies",
  CREATE_TRANSACTION: "transaction/create",
  GET_MONTHLY_BONUS: "bonus",
  GET_ALL_DOWNLINES: "user/downline/all",
  GET_PRODUCT_EXCELLENCE:  "product-excellence",
}

export async function getAllUser(){
  const res = await HttpService.get(ENDPOINT.GET_USER, null, {'Content-Type': 'application/json',});

  if(res.data && res.data.statusCode === 200){
    return res.data.message;
  }
  return "error when get all user data";
}

export async function getProductExcellence(){
  const res = await HttpService.get(ENDPOINT.GET_PRODUCT_EXCELLENCE, null, {'Content-Type': 'application/json',});

  if(res.data && res.data.statusCode === 200){
    return res.data.message;
  }
  return {
    error: true,
    errorMessage: "error when get all product excellence data",
  };
}