export const setHeader = (headers: any) =>{
  localStorage.setItem('content', headers.content);
  localStorage.setItem('exp', headers.exp);
}

export const getHeader = () =>{
  const content = localStorage.getItem('content');
  const exp = localStorage.getItem('exp');
  const headers = {
    content,
    exp,
    Authorization: 'Bearer '+content,
  }
  return headers;
}

export const setUser = (user: any) =>{
  localStorage.setItem('user', JSON.stringify(user));
}

export const getUser = () =>{
  const userString = localStorage.getItem('user');
  const user = userString? JSON.parse(userString): null;
  return user;
}

export const setMap = (map: any) =>{
  localStorage.setItem('map', JSON.stringify(map));
}

export const getMap = () =>{
  const mapString = localStorage.getItem('map');
  const map = mapString? JSON.parse(mapString): null;
  return map;
}

export const setStockies = (stockies: any) =>{
  localStorage.setItem('stockies', JSON.stringify(stockies));
}

export const getStockies = () =>{
  const stockiesString = localStorage.getItem('stockies');
  const stockies = stockiesString? JSON.parse(stockiesString): null;
  return stockies;
}