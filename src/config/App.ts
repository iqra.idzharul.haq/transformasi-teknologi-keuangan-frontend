import React from 'react';

export const CompanyName = 'TRANSFORMASI TEKNOLOGI KEUANGAN';
export const ProjectName = 'WEB APPLICATION';
export const MEMBER_PORTAL = 'http://member.berassemesta.com/';

export const DefaultLayout = React.lazy(() => import('../components/containers/DefaultLayout'));
