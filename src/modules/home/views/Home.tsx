import React, { Component, Fragment } from 'react';
import nojs from '../../../assets/img/nojs.png';
import logo from '../../../assets/img/brand/logo.png';
import ProductComponent from '../components/ProductComponent';
import ProductExcellence from '../components/ProductExcellence';
import PageTitle from '../../company/components/PageTitle';
import BenefitMember from '../components/BenefitMember';
import ContactForm from '../components/ContactForm';

class Home extends Component<any, any> {
  render() {
    return (
      <div>
          <div className="top-wrapper">
            <div className="container">
              <h1>TRANSFORMASI TEKNOLOGI KEUANGAN</h1>
              <h4>Transformasi Teknologi Keuangan Adalah <br></br>Perusahaan Penyedia Jasa Solusi Digital.</h4>
            </div>
          </div>
          <BenefitMember title="BISNIS KAMI"/>
          <ProductExcellence title="PRODUK KAMI"/>
          <ContactForm title="HUBUNGI KAMI"></ContactForm>
      </div>
    )
  }
}

export default Home;
