import React, { Component } from "react";
import { Row, Col, Input, Button } from "reactstrap";
import nojs from '../../../assets/img/nojs.png';

class ContactForm extends Component <any,any> {
  render() {
    const { title } = this.props;
    return (
      <div className="animated fadeIn pad-100">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row className="mar-bot-50">
            <Col className=" text-align-center">
              {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p> */}
            </Col>
          </Row>
          <Row>
            <Col lg="6" md="6">
                <p>Name</p>
                <Input placeholder="masukkan nama" type="text"/>
                <p>Email</p>
                <Input placeholder="masukkan email" type="text"/>
                <p>Messages</p>
                <Input type="textarea" placeholder="masukkan pesan" rows={5}></Input>
                <br/>
                <Button className="btn-default">Submit</Button>
              </Col>
              <Col lg="6" md="6">
                <img className="gambar-beside" src={nojs}/>
              </Col>
          </Row>
        </Col>
      </div>
    )
  }
}

export default ContactForm;