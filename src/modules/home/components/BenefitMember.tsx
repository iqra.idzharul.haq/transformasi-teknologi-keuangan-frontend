import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody } from "reactstrap";
// import product from '../../../assets/img/product.png';
import data from '../../../assets/img/data.jpg';
import web from '../../../assets/img/web.jpg';
import mobile from '../../../assets/img/mobile.png';

class BenefitMember extends Component <any,any> {
  state={
    data:[
      {
        name: 'Web Dev',
        description: 'Custom Website | Company Profile Enterprise Software and System',
        url: web,
      },
      {
        name: 'Mobile Dev',
        description: 'IOS Application | Android Application Cross-Platform Application',
        url: mobile,
      },
      {
        name: 'Data & Analytics',
        description: 'SEO Optimizer | Big Data Analysis Database Management',
        url: data,
      },
    ]
  }
  render() {
    const { title } = this.props;
    const { data } = this.state;
    return (
      <div className="animated fadeIn pad-100">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row className="mar-bot-50">
            <Col className=" text-align-center">
              <p>Bisnis yang dilakukan oleh Transformasi Teknologi Keuangan</p>
            </Col>
          </Row>
          <Row>
            {
              data && data.map((item)=>{
                return(
                  <Col className=" text-align-center">
                    <Card className="min-height-300 p-4">
                      <CardHeader>
                        <h3>{item.name}</h3>
                      </CardHeader>
                      <CardBody>
                      <div className="member-icon mar-bot-30">
                        <img src={item.url}/>
                      </div>
                      <p>{item.description}</p>
                      </CardBody>
                    </Card>
                  </Col>
                )
              })
            }
          </Row>
        </Col>
      </div>

      // <div className="member">
      //   <div className="member-title">
      //     <h2>{title}</h2>
      //     <h3>Member transformasi teknologi keuangan akan mendapatkan Keuntungan atas Penjualan Langsung kepada Konsumen (non member). Keuntungan ini didapat dari selisih harga jual produk Member ke Konsumen (non member)</h3>
      //   </div>
      //   <div className="member-body">
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Member</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus dari member!</p>
          // </div>
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Belanja</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus belanja dari pembelanjaan pribadi anda!</p>
          // </div>
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Level</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus dari setiap member yang anda ajak!</p>
          // </div>
      //     <div className="clear"></div>
      //   </div>
      // </div>
    )
  }
}

export default BenefitMember;