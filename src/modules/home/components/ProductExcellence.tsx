import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { getProductExcellence } from "../../../utilities/api";

class ProductExcellence extends Component <any,any> {
  state={
    left:[
      {
        name: 'E-Commerce',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      },
      {
        name: 'Digital Payment',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      },
    ],
    right:[
      {
        name: 'Digital Transaction',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      },
      {
        name: 'Enterprise System',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      },
    ]
  }

  render() {
    const { title } = this.props;
    const { left, right } = this.state;
    return(
      <div className="product-exellence pad-100-50">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row>
            <Col className=" text-align-center">
              <p>Keunggulan dari produk-produk yang dipasarkan oleh Transformasi Teknologi Keuangan</p>
            </Col>
          </Row>
        </Col>
        <Row>
          <Col className="text-align-right">
          {
            left && left.map((item, index)=>{
              const {name, description} = item
              return(
                <div className="body-excel" key={name}>
                  <span className="fa fa-check fa-2x">&nbsp; {name}</span>
                  <p>{description}</p>
                </div>
              )
            })
          }
          </Col>
          <Col className="text-align-left">
          {
            right && right.map((item, index)=>{
              const {name, description} = item
              return(
                <div className="body-excel" key={name}>
                  <span className="fa fa-check fa-2x">&nbsp; {name}</span>
                  <p>{description}</p>
                </div>
              )
            })
          }
          </Col>
        </Row>
      </div>
    )
  }
}

export default ProductExcellence;